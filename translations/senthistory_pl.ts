<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Shortcuts</source>
        <translation>Skróty</translation>
    </message>
    <message>
        <source>Chat Window</source>
        <translation>Okno rozmowy</translation>
    </message>
    <message>
        <source>Sent messages&apos; history</source>
        <translation>Historia wysłanych wiadomości</translation>
    </message>
    <message>
        <source>Previous message</source>
        <translation>Poprzednia wiadomość</translation>
    </message>
    <message>
        <source>Next message</source>
        <translation>Następna wiadomość</translation>
    </message>
    <message>
        <source>Previous message from all chats</source>
        <translation>Poprzednia wiadomość ze wszystkich rozmów</translation>
    </message>
    <message>
        <source>Next message from all chats</source>
        <translation>Następna wiadomość ze wszystkich rozmów</translation>
    </message>
</context>
</TS>
