<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <source>Shortcuts</source>
        <translation>Shortcuts</translation>
    </message>
    <message>
        <source>Chat Window</source>
        <translation>Chat Window</translation>
    </message>
    <message>
        <source>Sent messages&apos; history</source>
        <translation>Sent messages&apos; history</translation>
    </message>
    <message>
        <source>Previous message</source>
        <translation>Previous message</translation>
    </message>
    <message>
        <source>Next message</source>
        <translation>Next message</translation>
    </message>
    <message>
        <source>Previous message from all chats</source>
        <translation>Previous message from all chats</translation>
    </message>
    <message>
        <source>Next message from all chats</source>
        <translation>Next message from all chats</translation>
    </message>
</context>
</TS>
