<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Shortcuts</source>
        <translation>Zkratky</translation>
    </message>
    <message>
        <source>Chat Window</source>
        <translation>Okno pro rozhovor</translation>
    </message>
    <message>
        <source>Sent messages&apos; history</source>
        <translation>Poslat histori zpráv</translation>
    </message>
    <message>
        <source>Previous message</source>
        <translation>Předchozí zpráva</translation>
    </message>
    <message>
        <source>Next message</source>
        <translation>Další zpráva</translation>
    </message>
    <message>
        <source>Previous message from all chats</source>
        <translation>Předchozí zpráva ze všech rozhovorů</translation>
    </message>
    <message>
        <source>Next message from all chats</source>
        <translation>Další zpráva ze všech rozhovorů</translation>
    </message>
</context>
</TS>
