<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="tr">
<context>
    <name>@default</name>
    <message>
        <source>Shortcuts</source>
        <translation>Kısayollar</translation>
    </message>
    <message>
        <source>Chat Window</source>
        <translation>Konuşma Penceresi</translation>
    </message>
    <message>
        <source>Sent messages&apos; history</source>
        <translation>Mesajlaşma geçmişini gönder</translation>
    </message>
    <message>
        <source>Previous message</source>
        <translation>Önceki mesaj</translation>
    </message>
    <message>
        <source>Next message</source>
        <translation>Sonraki mesaj</translation>
    </message>
    <message>
        <source>Previous message from all chats</source>
        <translation>Bütün konuşmalardan önceki mesaj</translation>
    </message>
    <message>
        <source>Next message from all chats</source>
        <translation>Bütün konuşmalardan sonraki mesaj</translation>
    </message>
</context>
</TS>
